package com.hobbitapi;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

@Repository
public class HobbitRepository {
    @PersistenceContext
    private EntityManager entityManager;

    public Hobbit save(Hobbit hobbit) {
        entityManager.persist(hobbit);
        return hobbit;
    }

    public Hobbit findById(Long id) {
        return entityManager.find(Hobbit.class, id);
    }
}
