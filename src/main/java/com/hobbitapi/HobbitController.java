package com.hobbitapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/hobbits")
public class HobbitController {

    @Autowired
    private HobbitRepository hobbitRepository;

    @PostMapping
    public ResponseEntity<Hobbit> createHobbit(@RequestBody Hobbit hobbit) {
        Hobbit savedHobbit = hobbitRepository.save(hobbit);
        return ResponseEntity.ok(savedHobbit);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Hobbit> getHobbit(@PathVariable Long id) {
        Hobbit hobbit = hobbitRepository.findById(id);
        if (hobbit == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(hobbit);
    }

    // Query a hobbit by name?
}
